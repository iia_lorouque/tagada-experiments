(
  version: (0, 6, 2),
  versioning: (
    url: "https://gitlab.limos.fr/iia_lorouque/tagada",
    commit: Some("e3f8cb5b177cee921fff499281ddee486c88fcbe"),
  ),
  commands: (
    build: "echo OK",
  ),
  aliases: {
    "TAGADA_HOME": "/home/default/tagada",
    "TAGADA_TOOLS": "/home/default/tagada/tagada-tools/target/release/tagada-tools",
    "CIPHER": "rijndael",
    "CIPHERS_DIR": "{TAGADA_HOME}/tagada-specs/ciphers",
    "TRUNCATION_OPTIONS": "--generate-xors 5 --diff-variables --transitivity",
  },
  experiments: [
    (
      foreach: {
        "PLAINTEXT_SIZE": [ 128, 160, 192, 224, 256 ],
        "KEY_SIZE":       [ 128, 160, 192, 224, 256 ],
        "ROUND": ( start: 3, end_inclusive: 14 ),
      },
      where: [
        "PLAINTEXT_SIZE == 128 && KEY_SIZE == 128 && ROUND <= 5",
        "PLAINTEXT_SIZE == 128 && KEY_SIZE == 160 && ROUND <= 8",
        "PLAINTEXT_SIZE == 128 && KEY_SIZE == 192 && ROUND <= 9",
        "PLAINTEXT_SIZE == 128 && KEY_SIZE == 224 && ROUND <= 12",
        "PLAINTEXT_SIZE == 128 && KEY_SIZE == 256 && ROUND <= 14",

        "PLAINTEXT_SIZE == 160 && KEY_SIZE == 128 && ROUND <= 4",
        "PLAINTEXT_SIZE == 160 && KEY_SIZE == 160 && ROUND <= 6",
        "PLAINTEXT_SIZE == 160 && KEY_SIZE == 192 && ROUND <= 8",
        "PLAINTEXT_SIZE == 160 && KEY_SIZE == 224 && ROUND <= 9",
        "PLAINTEXT_SIZE == 160 && KEY_SIZE == 256 && ROUND <= 11",

        "PLAINTEXT_SIZE == 192 && KEY_SIZE == 128 && ROUND <= 3",
        "PLAINTEXT_SIZE == 192 && KEY_SIZE == 160 && ROUND <= 5",
        "PLAINTEXT_SIZE == 192 && KEY_SIZE == 192 && ROUND <= 7",
        "PLAINTEXT_SIZE == 192 && KEY_SIZE == 224 && ROUND <= 8",
        "PLAINTEXT_SIZE == 192 && KEY_SIZE == 256 && ROUND <= 10",

        "PLAINTEXT_SIZE == 224 && KEY_SIZE == 128 && ROUND <= 3",
        "PLAINTEXT_SIZE == 224 && KEY_SIZE == 160 && ROUND <= 4",
        "PLAINTEXT_SIZE == 224 && KEY_SIZE == 192 && ROUND <= 6",
        "PLAINTEXT_SIZE == 224 && KEY_SIZE == 224 && ROUND <= 7",
        "PLAINTEXT_SIZE == 224 && KEY_SIZE == 256 && ROUND <= 9",

        "PLAINTEXT_SIZE == 256 && KEY_SIZE == 128 && ROUND <= 3",
        "PLAINTEXT_SIZE == 256 && KEY_SIZE == 160 && ROUND <= 4",
        "PLAINTEXT_SIZE == 256 && KEY_SIZE == 192 && ROUND <= 5",
        "PLAINTEXT_SIZE == 256 && KEY_SIZE == 224 && ROUND <= 5",
        "PLAINTEXT_SIZE == 256 && KEY_SIZE == 256 && ROUND <= 8",
      ],
      apply: (
        aliases: {
          "id.main": "plaintext-{PLAINTEXT_SIZE}_key-{KEY_SIZE}_round-{ROUND}",
          "spec":  "{id.main}.spec",
          "diff":  "{id.main}.diff",
          "trunc": "{id.main}.trunc",
        },
        cmds: [
          ( name: "{spec}",  cmd: "bundle exec --gemfile {TAGADA_HOME}/tagada-specs/Gemfile {CIPHERS_DIR}/{CIPHER}.rb --plainsize {PLAINTEXT_SIZE} --keysize {KEY_SIZE} --round {ROUND} > {spec}.json" ),
          ( name: "{diff}",  cmd: "{TAGADA_TOOLS} transform derive {spec}.json > {diff}.json" ),
          ( name: "{trunc}", cmd: "{TAGADA_TOOLS} transform truncate-differential {diff}.json {TRUNCATION_OPTIONS} shared --port 1024 > {trunc}.json" ),
          (
            foreach: {
              "BACKEND_SOLVER": [ "picat" ], 
            },
            apply: (
              aliases: {
                "id.search": "{id.main}_solver-{BACKEND_SOLVER}",
                "search-best-dc": "{id.search}.search-best-dc",
              },
              cmds: [
                ( name: "{search-best-dc}", cmd: "{TAGADA_TOOLS} search best-differential-characteristic --encoding cnf {diff}.json {trunc}.json {BACKEND_SOLVER} > {search-best-dc}.json" ),
              ]
            )
          )
        ]
      )
    )
  ]
)
