#!/usr/bin/bash

export BARGS_VARS_PATH="docker_run.bargs"
source "${PWD}"/"$(dirname ${BASH_SOURCE[0]})"/bargs.sh "$@"

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Docker helpers        
  is_container() {
    [ "$(docker ps --all --quiet --filter name="$1")" != '' ]
  }                                                               
  is_container_created() {
    [ "$(docker ps --all --quiet --filter name="$1" --filter status=created)" != '' ]
  }
  
  is_container_running() {
    [ "$(docker ps --all --quiet --filter name="$1" --filter status=running)" != '' ]
  }
  
  is_network() {
    [ "$(docker network ls --quiet --filter name="$1")" != '' ]
  }
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Input helpers                                                                      
  function ask_yes_no {
    local QUESTION="$1"
    local DEFAULT="$2"
    if [ "$DEFAULT" = true ]; then
      OPTIONS='[Y/n]'
      DEFAULT='y'
    else
      OPTIONS='[y/N]'
      DEFAULT='n'
    fi
    read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
    local INPUT=${INPUT:-${DEFAULT}}
    echo "${INPUT}"
    if [[ "$INPUT" =~ ^[yY]$ ]]; then
      return 0 # True
    else
      return 1 # False
    fi
  }
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

tagada_daemon="tagada-daemon-$tagada_daemon_version"

mkdir -p "$(pwd)/experiment-results"

if ! is_network "tagada_network"; then
  echo "Creating tagada_network"
  docker network create "tagada_network"
fi

create_container() {
  docker container create -it \
      --network tagada_network \
      --mount type=bind,source="$(pwd)/experiment-results",target=/home/default/experiments \
      --name "$container_name" \
      "$image"
}

if is_container "$container_name"; then
  echo A container with the name "'$container_name'" already exists.
  if ask_yes_no 'Would you replace the old container by a new container? (If the container is running it will be stopped)' false; then
    if is_container_running "$container_name"; then
      docker stop "$container_name"
    fi
    docker rm "$container_name"
    create_container
  fi
else
  create_container
fi

if is_container "$tagada_daemon"; then
  echo The container "'$tagada_daemon'" already exists.
  if ! is_container_running "$tagada_daemon"; then
    echo Starting the "'$tagada_daemon'" container.
    docker start "$tagada_daemon"
    # docker exec "$tagada_daemon"
  fi
else
  echo Creating and running the "'$tagada_daemon'" container.
  docker run --network-alias tagada-daemon-abstractor --network tagada_network --name "$tagada_daemon" "$tagada_daemon" &
fi

mkdir -p "$(dirname "experiment-results/$target_configuration")"
cp "$configuration_file" "experiment-results/$target_configuration"
docker start "$container_name"
docker exec  "$container_name" whitesmith "/home/default/experiments/$target_configuration" build
docker exec  "$container_name" whitesmith "/home/default/experiments/$target_configuration" run \
  --nb-threads "$num_threads" \
  $(if_set "$timeout" --global-timeout "$timeout") 
docker stop  "$container_name"
if [ "$remove" = "true" ]; then
  docker rm "$container_name"
fi