#!/usr/bin/bash

export BARGS_VARS_PATH="main.bargs"
source "${PWD}"/"$(dirname ${BASH_SOURCE[0]})"/bargs.sh "$@"

target_file="$(echo "${configuration_file##"$relative_to"}" | sed -r "s#^/**##")"
commit_version="$(grep 'commit:' < "$configuration_file" | sed -re 's/.*"(.*)".*/\1/g')"
whitesmith_version="$(grep 'version:' < "$configuration_file" | sed -re "s#.*\( *([0-9]+) *, *([0-9]+) *, *([0-9]+) *\).*#\1.\2.\3#")"
image_id="tagada-$commit_version-$whitesmith_version"
container_name="$(echo "$target_file" | tr '/' '_')"
container_name="${container_name%%.ron}"

bash docker_build.sh \
  --tagada_version "$commit_version" \
  --whitesmith_version "$whitesmith_version" \
  --image "$image_id" \
  --tagada_daemon_version "$tagada_daemon_version"

bash docker_run.sh \
  --configuration_file "$configuration_file" \
  --image "$image_id" \
  --container_name "$container_name" \
  --target_configuration "$target_file" \
  --num_threads "$num_threads" \
  --tagada_daemon_version "$tagada_daemon_version" \
  --timeout "$timeout" \
  "$(flag "$remove" --remove)"