FROM ruby:3.2-bookworm

# Defining default non-root user UID, GID, and name
ARG USER_UID="1000"
ARG USER_GID="1000"
ARG USER_NAME="default"

# Creating default non-user
RUN groupadd -g $USER_GID $USER_NAME\
    && useradd -m -g $USER_GID -u $USER_UID $USER_NAME

# Installing basic packages
RUN apt-get update\
    && apt-get install -y zip unzip curl locales locales-all git build-essential libc6 python3 vim nano
RUN apt-get install -y  rbenv

# Set UTF-8 as default
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# Switching to non-root user to install SDKMAN!
USER $USER_UID:$USER_GID

# Downloading SDKMAN!
RUN curl -s "https://get.sdkman.io" | bash
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk update"

# Installing Gradle
ARG GRADLE_VERSION="7.5.1"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install gradle $GRADLE_VERSION"
ENV GRADLE_HOME="/home/default/.sdkman/candidates/gradle/current"
ENV PATH="$GRADLE_HOME/bin:$PATH"

# Installing Java
ARG JAVA_VERSION="17.0.5-tem"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install java $JAVA_VERSION"
ENV JAVA_HOME="/home/default/.sdkman/candidates/java/current"
ENV PATH="$JAVA_HOME/bin:$PATH"

# Installing Kotlin
ARG KOTLIN_VERSION="1.8.20"
ENV KOTLIN_HOME="/home/default/.sdkman/candidates/kotlin/current"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install kotlin $KOTLIN_VERSION"
ENV PATH="$KOTLIN_HOME/bin:$PATH"

# Install rbenv
RUN echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >> ~/.bashrc
RUN echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >> ~/.profile
ENV PATH "/home/default/.rbenv/shims:/home/default/.rbenv/bin:$PATH"

# Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH "/home/default/.cargo/bin:$PATH"

ARG TAGADA_VERSION="master"
# Installing Tagada
RUN cd /home/default \
    && git clone https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lulibral/tagada.git tagada \
    && cd tagada \
    && git checkout $TAGADA_VERSION

RUN cd /home/default/tagada \
    && git clone https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tagada-specs.git tagada-specs \
    && cd tagada-specs \
    && git pull origin master \
    && bundle install \
    && chmod +x /home/default/tagada/tagada-specs/test_vectors/*

RUN cd /home/default/tagada \
    && git clone https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lulibral/tagada-lib.git tagada-tools \
    && cd tagada-tools \
    && git pull origin master \
    && cargo build --release \
    && mv operator_info_store-1.0.0.mpk ..

RUN cd /home/default/tagada \
    && git clone https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tadaga-step2.git tagada-step2 \
    && cd tagada-step2 \
    && git pull origin main \
    && gradle shadowJar

RUN cd /home/default/tagada \ 
    && git clone https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tagada-deps.git tagada-deps \
    && cd tagada-deps \
    && git pull origin master \
    && chmod +x install_minizinc.sh \
    && ./install_minizinc.sh \
    && chmod +x /home/default/tagada/tagada-deps/MiniZinc/latest/bin/* \
    && chmod +x install_picat.sh \
    && ./install_picat.sh \
    && chmod +x /home/default/tagada/tagada-deps/Picat/latest/picat \
    && chmod +x /home/default/tagada/tagada-deps/espresso
ENV PATH "/home/default/tagada/tagada-deps/Picat/latest:$PATH"
ENV PATH "/home/default/tagada/tagada-deps:$PATH"

ARG WHITESMITH_VERSION="latest"
RUN cd /home/default/tagada \
    && cargo install --git https://github.com/rloic/whitesmith --tag $WHITESMITH_VERSION
ENV PATH "/home/default/.cargo/bin:$PATH"

RUN mkdir -p /home/default/experiments

# Global library
ENV TAGADA_HOME=/home/default/tagada

# Specs
ENV SPECS=$TAGADA_HOME/tagada-specs
ENV BUNDLE_GEMFILE=$SPECS/Gemfile

# Tools
ENV TOOLS=$TAGADA_HOME/tagada-tools/target/release/tagada-tools

# Step2
ENV STEP2=$TAGADA_HOME/tagada-step2/build/libs/tagada-step2-1.0.0-all.jar

# External dependencies
ENV DEPS=$TAGADA_HOME/tagada-deps

ENV MINIZINC=$DEPS/MiniZinc/latest/bin/minizinc

ENV PICAT=$DEPS/Picat/latest/picat
ENV FZN_PICAT_SAT=$DEPS/Picat/latest/lib/fzn_picat_sat.pi

ENV ESPRESSO_AB=/home/default/tagada/tagada-deps/espresso

# ENV GUROBI_DLL=
# ENV FZN_ORTOOLS=

WORKDIR /home/default/tagada
