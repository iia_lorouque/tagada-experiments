#!/usr/bin/bash

export BARGS_VARS_PATH="docker_build.bargs"
source "${PWD}"/"$(dirname ${BASH_SOURCE[0]})"/bargs.sh "$@"

tagada_daemon="tagada-daemon-$tagada_daemon_version"

docker build \
  --build-arg TAGADA_VERSION="$tagada_version" \
  --build-arg WHITESMITH_VERSION="$whitesmith_version" \
  --build-arg USER_UID="$(id -u)" \
  --build-arg USER_GID="$(id -g)" \
  --tag "$image" \
  .

cd daemon-abstractor || exit 1

docker build \
  --build-arg USER_UID="$(id -u)" \
  --build-arg USER_GID="$(id -g)" \
  --build-arg TAGADA_DAEMON_VERSION="$tagada_daemon_version" \
  --tag "$tagada_daemon" \
  .
